#!/bin/bash

# Mengambil angka jam saat ini
Hour=$(date +%k)

if [ "$Hour" == "0" ]
then 
  img_total=1
else
  img_total=$Hour
fi

# Download gambar sebanyak angka pada jam saat ini

make_img(){
i=1
for ((i=1; i<=img_total; i++))
do 
	wget -O "perjalanan_$i.jpg" "https://loremflickr.com/320/240/Indonesia"
done
}

make_zip(){
i=1

while [ -d "kumpulan_$i" ]
do
mkdir "devil_$i"
zip -r "devil_$i.zip" "kumpulan_$i"
rm -r "devil_$i"
i=$((i + 1))
done 
}

dwn_img(){
i=1
j=$((i + 1))
echo "$j"
while [ $i -le $j ]
do 
 if [ ! -d "kumpulan_$i" ] 
 then 
   mkdir "kumpulan_$i"
   cd "kumpulan_$i"
   make_img
 fi
 if [ -d "kumpulan_$i" ] 
 then
   mkdir "kumpulan_$j"
   cd "kumpulan_$j"
   make_img
   j=$((i + 1))
 fi
i=$((i + 1))
done
cd ..
}




delete_folder(){
i=1
while [ -d "kumpulan_$i" ] 
do 
 rm -r "kumpulan_$i"
i=$((i + 1)) 
done
}




while true 
do
echo "=== Soal 2 ==="
echo "Pilih opsi dibawah ini"
echo "1. Download"
echo "2. Zip"
echo "3. Delete Folder"
echo "4. Keluar Program"
read -p "Masukkan jawaban : " pilihan
case "$pilihan" in
   "1") dwn_img
   ;;
   "2") make_zip
   ;; 
   "3") delete_folder
   ;; 
   "4") exit 1
   ;; 
   *)   echo "PIlihan tidak valid"
   ;;
  esac
done

